<?php
/**
 * @file
 * Accordion formatter for Field collection group.
 */

class CollectionFormatterAccordion extends CollectionFormatter {
  protected $groupClass = 'CollectionGroupAccordion';

  /**
   * Formatter info for the accordion field collection formatter.
   */
  public function info() {
    $info = array(
      'label' => t('Collections grouped in Accordion'),
    ) + parent::info();
    $info['settings']['expand_top_item'] = FALSE;
    return $info;
  }

  /**
   * Formatter settings form for the accordion field collection formatter.
   */
  public function settingsForm($field, $settings) {
    $element = parent::settingsForm($field, $settings);
    $element['expand_top_item'] = array(
      '#type' => 'checkbox',
      '#title' => t('Expand top item'),
      '#default_value' => $settings['expand_top_item'],
    );
    return $element;
  }

  /**
   * Formatter settings summary for the fieldset field collection formatter.
   */
  public function settingsSummary($settings) {
    $summary = parent::settingsSummary($settings);
    if ($settings['expand_top_item']) {
      $summary[] = t('Top item is expanded');
    }
    return $summary;
  }
}
