<?php
/**
 * @file
 * Accordion group of the field collection grouping functionality.
 */

class CollectionGroupAccordion extends CollectionGroup {
  protected $valueFormat = 'accordion_item';
  protected $wrapperFomat = 'accordion';

  /**
   * Add accordion-specific settings to the wrapper group.
   */
  protected function wrapperGroup() {
    $group = parent::wrapperGroup();
    $group->format_settings['instance_settings']['speed'] = 'fast';
    $group->format_settings['instance_settings']['effect'] = 'bounceslide';
    return $group;
  }

  /**
   * Label markup for the accordion item header.
   */
  protected function labelMarkup(&$tab, stdClass $group) {
    $active = !empty($this->settings['expand_top_item']) && $group->weight === 1;

    $active_class = $active ? ' field-group-accordion-active' : '';
    return '<h3 class="field-group-format-toggler accordion-item' . $active_class . '"><a href="#">' . $tab['#title'] . '</a></h3>';
  }

  /**
   * Override the rendering of the group for individual accordion items.
   */
  protected function valuePreRender(&$tab, stdClass $group, &$element) {
    $this->ensureTitle($tab);

    $label_markup = $this->labelMarkup($tab, $group);
    $tab += array(
      '#type' => 'markup',
      '#prefix' => $label_markup . '<div class="field-group-format-wrapper">',
      '#suffix' => '</div>',
    );
  }

  /**
   * Add accordion-specific javascript.
   */
  protected function addAttached(&$attached) {
    $attached['js'][] = drupal_get_path('module', 'field_group') . '/field_group.js';
  }
}
